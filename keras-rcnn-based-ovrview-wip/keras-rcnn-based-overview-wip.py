import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from glob import glob
import os
from skimage.io import imread
import matplotlib.pyplot as plt
import seaborn as sns
from skimage.morphology import label
from skimage.measure import regionprops
import keras.backend
import keras.preprocessing.image
import numpy
import skimage.io
import skimage.transform
import matplotlib
import keras
from keras.layers import Input
from keras_rcnn.models import RCNN
from skimage.morphology import closing, opening, disk
from skimage.morphology import label # label regions
from .DictionaryIterator import DictionaryIterator, ImageSegmentationGenerator
from .process_image import *
from .read_data import *



seg_gen = ImageSegmentationGenerator().flow(train_dict, {'nucleus': 1})
x_out, _ = seg_gen.next()


(target_bounding_boxes, target_image, _, _, target_scores) = x_out
target_bounding_boxes = np.squeeze(target_bounding_boxes)
target_image = np.squeeze(target_image)
target_scores = np.argmax(target_scores, -1)
target_scores = np.squeeze(target_scores)


def get_rcnn_model():
    img_in = Input((None, None, 3))
    rcnn_model = RCNN(img_in, 2)
    optimizer = keras.optimizers.Adam(0.0001)
    rcnn_model.compile(optimizer)
    return rcnn


rcnn_model.fit_generator(fit_gen(sg_gen),
                        steps_per_epoch = 20,
                        epochs = 2)

test_ds = next(fit_gen())
y_anchors, y_scores = rcnn_model.predict(test_ds[0])
#print(y_anchors.shape, y_scores.shape)

target_bounding_boxes = np.squeeze(y_anchors)
target_image = np.squeeze(test_ds[0][1][0])
target_scores = np.argmax(np.squeeze(y_scores), -1)



test_df = img_df.query('TrainingSplit=="test"')
test_rows = []
group_cols = ['Stage', 'ImageId']
for n_group, n_rows in test_df.groupby(group_cols):
    c_row = {col_name: col_value for col_name, col_value in zip(group_cols, n_group)}
    c_row['images'] = n_rows.query('ImageType == "images"')['path'].values.tolist()
    test_rows += [c_row]
test_img_df = pd.DataFrame(test_rows)    

test_img_df['images'] = test_img_df['images'].map(read_and_stack).map(lambda x: x[:,:,:IMG_CHANNELS])
#print(test_img_df.shape[0], 'images to process')
#test_img_df.sample(1)

test_img_df['masks'] = test_img_df['images'].map(lambda x: simple_cnn.predict(np.expand_dims(x, 0))[0, :, :, 0])


_, train_rle_row = next(train_img_df.tail(5).iterrows()) 
train_row_rles = list(prob_to_rles(train_rle_row['masks']))

tl_rles = train_labels.query('ImageId=="{ImageId}"'.format(**train_rle_row))['EncodedPixels']

match, mismatch = 0, 0
for img_rle, train_rle in zip(sorted(train_row_rles, key = lambda x: x[0]), 
                             sorted(tl_rles, key = lambda x: x[0])):
    for i_x, i_y in zip(img_rle, train_rle):
        if i_x == i_y:
            match += 1
        else:
            mismatch += 1
print('Matches: %d, Mismatches: %d, Accuracy: %2.1f%%' % (match, mismatch, 100.0*match/(match+mismatch)))

test_img_df['rles'] = test_img_df['masks'].map(clean_img).map(lambda x: list(prob_to_rles(x)))

out_pred_list = []
for _, c_row in test_img_df.iterrows():
    for c_rle in c_row['rles']:
        out_pred_list+=[dict(ImageId=c_row['ImageId'], 
                             EncodedPixels = ' '.join(np.array(c_rle).astype(str)))]
out_pred_df = pd.DataFrame(out_pred_list)
print(out_pred_df.shape[0], 'regions found for', test_img_df.shape[0], 'images')
out_pred_df.sample(3)

out_pred_df[['ImageId', 'EncodedPixels']].to_csv('predictions.csv', index = False)


