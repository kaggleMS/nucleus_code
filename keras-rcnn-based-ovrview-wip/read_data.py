import numpy as np
from skimage.measure import regionprops



IMG_CHANNELS = 3


def read_and_stack(in_img_list):
    return np.sum(np.stack([imread(c_img) for c_img in in_img_list], 0), 0)/255.0


def read_and_stack_masks(in_img_list):
    return np.sum(np.stack([i*(imread(c_img)>0) for i, c_img in 
                            enumerate(in_img_list,1)], 0), 0)


def read_data():
    #%matplotlib inline
    dsb_data_dir = os.path.join('..', 'input')
    stage_label = 'stage1'

    train_labels = pd.read_csv(os.path.join(dsb_data_dir,'{}_train_labels.csv'.format(stage_label)))
    train_labels['EncodedPixels'] = train_labels['EncodedPixels'].map(lambda ep: [int(x) for x in ep.split(' ')])
    #train_labels.sample(3)

    all_images = glob(os.path.join(dsb_data_dir, 'stage1_*', '*', '*', '*'))
    img_df = pd.DataFrame({'path': all_images})
    img_id = lambda in_path: in_path.split('/')[-3]
    img_type = lambda in_path: in_path.split('/')[-2]
    img_group = lambda in_path: in_path.split('/')[-4].split('_')[1]
    img_stage = lambda in_path: in_path.split('/')[-4].split('_')[0]
    img_df['ImageId'] = img_df['path'].map(img_id)
    img_df['ImageType'] = img_df['path'].map(img_type)
    img_df['TrainingSplit'] = img_df['path'].map(img_group)
    img_df['Stage'] = img_df['path'].map(img_stage)
    #img_df.sample(2)

    train_df = img_df.query('TrainingSplit=="train"')
    train_rows = []
    group_cols = ['Stage', 'ImageId']
    for n_group, n_rows in train_df.groupby(group_cols):
        c_row = {col_name: col_value for col_name, col_value in zip(group_cols, n_group)}
        c_row['mask_paths'] = n_rows.query('ImageType == "masks"')['path'].values.tolist()
        c_row['image_paths'] = n_rows.query('ImageType == "images"')['path'].values.tolist()
    
        train_rows += [c_row]
    train_img_df = pd.DataFrame(train_rows)    
    train_img_df['images'] = train_img_df['image_paths'].map(read_and_stack).map(lambda x: x[:,:,:IMG_CHANNELS])

    train_img_df['masks'] = train_img_df['mask_paths'].map(read_and_stack_masks).map(lambda x: x.astype(int))
    #train_img_df['images'].map(lambda x: x.shape).value_counts()
    #train_img_df.sample(1)

    train_dict = [dict(image = dict(
        pathname = c_row['image_paths'][0],
                      shape =  dict(zip(['r', 'c', 'channels'], c_row['images'].shape))),
                       objects = [{'bounding_box' : dict(
                        minimum = dict(r = c_reg.bbox[0], c = c_reg.bbox[1]),
                       maximum = dict(r = c_reg.bbox[2], c = c_reg.bbox[3])),
                                      'class' : "nucleus",
                                   'mask': dict(pathname = c_path)
                                  }
                                       for c_reg, c_path in zip(
                                           regionprops(label(c_row['masks'])),
                                           c_row['mask_paths']
                                       )]
                      ) 
                  for _, c_row in train_img_df.iterrows()]
    return train_img_df