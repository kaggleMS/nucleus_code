from scipy import misc
import readline, glob
import matplotlib.pyplot as plt
import os
filelist = glob.glob('*.png')
from tqdm import tqdm

def join_masks(folder_path):
    filelist = glob.glob(folder_path + '/masks/*.png')
    masks = []
    for path in filelist:
        mask = misc.imread(path)
        masks.append(mask)
    if masks:
     joined_mask = masks[0]
     if len(mask)>=2:
      for mask in masks[1:]:
        joined_mask += mask
     misc.imsave(folder_path + '/joined_mask.png', joined_mask)     
    return

def main():
    for folder_path in tqdm(glob.glob('*')):
        join_masks(folder_path)
    return
'''
plt.imshow(face)
plt.show()
face2 = misc.imread('da13087009f78e4dd5bd93805ee1869a3b7f524a0ea87f1419481430be7a3bb4.png')
face2
face+face2
face3 = face+face2
plt.imshow(face3)
plt.show()
misc.imsave('face3.png', face3)
glob.glob('*')
filelist
'''

if __name__=='__main__':
    main()
