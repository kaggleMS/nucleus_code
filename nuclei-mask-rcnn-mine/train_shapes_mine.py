from skimage.transform import rescale, resize, downscale_local_mean
import json
import os
import sys
import random
import math
import re
import time
import numpy as np
import cv2
import matplotlib
import matplotlib.pyplot as plt

from nuclei_config import Config
import utils
import model as modellib
#import visualize
from model import log
from Dataset import ShapesDataset
import glob
import skimage
import pickle





#%matplotlib inline 

# Root directory of the project
ROOT_DIR = os.getcwd()

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)
    
class ShapesConfig(Config):
    NAME = "shapes"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 1  # background + 3 shapes
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 128
    IMAGE_SHAPE = [128,128]
    RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)  # anchor side in pixels
    TRAIN_ROIS_PER_IMAGE = 8
    STEPS_PER_EPOCH = 100
    VALIDATION_STEPS = 5
    MEAN_PIXEL = (3,)

class InferenceConfig(ShapesConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

inference_config = InferenceConfig()


    
def get_ax(rows=1, cols=1, size=8):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.
    
    Change the default size attribute to control the size
    of rendered images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    return ax


def main(root_dir, root_test_dir):
    config = ShapesConfig()
    dataset_train = ShapesDataset()
    dataset_train.load_shapes(root_dir, config.IMAGE_SHAPE[0], config.IMAGE_SHAPE[1])
    dataset_train.prepare()

    dataset_val = ShapesDataset()
    dataset_val.load_shapes(root_dir, config.IMAGE_SHAPE[0], config.IMAGE_SHAPE[1])
    dataset_val.prepare()


    dataset_test = ShapesDataset()
    dataset_test.load_shapes(root_test_dir, config.IMAGE_SHAPE[0], config.IMAGE_SHAPE[1])
    dataset_test.prepare()



    model = modellib.MaskRCNN(mode="training", config=config,
                          model_dir=MODEL_DIR)

    model.train(dataset_train, dataset_val, 
            learning_rate=config.LEARNING_RATE, 
            epochs=1, 
            layers='heads')

    model.train(dataset_train, dataset_val, 
            learning_rate=config.LEARNING_RATE / 10,
            epochs=2, 
            layers="all")
    model.keras_model.save_weights("mask_rcnn_shapes.h5")
    '''
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")
    '''

    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                          config=inference_config,
                          model_dir=MODEL_DIR)

    IMAGE_DIR = os.path.join(root_test_dir, "images")
    #file_names = next(os.walk(IMAGE_DIR))[2]
    file_names = glob.glob(root_test_dir+'/*/images/*.png')
    for file_name in file_names:
        #image = skimage.io.imread(file_name)
        image = resize(skimage.io.imread(file_name)[:,:,:3], (128, 128))
        results = model.detect([image], verbose=1)
        r = results[0]
        with open(file_name+'_res.pickle', 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(r, f, pickle.HIGHEST_PROTOCOL)
    print("Saved model to disk")
    
    return
import sys

main(sys.argv[1], sys.argv[2])
