import os
import sys
import random
import math
import re
import time
import numpy as np
import cv2
import matplotlib
import matplotlib.pyplot as plt
import glob
from tqdm import tqdm

from nuclei_config import ShapesConfig# as Config
from Dataset import Dataset, ShapesDataset
import utils
import model as modellib
import visualize
from model import log
import skimage



# Root directory of the project
ROOT_DIR = os.getcwd()

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed

if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)

config = ShapesConfig()
#config.display()

dataset_nuclei_train = Dataset()
dataset_nuclei_mask_train = Dataset()

dataset_nuclei_val = Dataset()

paths = glob.glob('../../input/stage1_train/*/images/*.png')
mask_paths = glob.glob('../../input/stage1_train/*/masks/*.png')

n = 0
for path_i in tqdm(paths):
    dataset_nuclei_train.add_image('source',
     n,path_i,bg_color = np.array([random.randint(0, 0) for _ in range(3)]),
     height=256,width=256)
    n += 1

n = 0
for path_i in tqdm(paths):
    dataset_nuclei_val.add_image('source',
     n,path_i,bg_color = np.array([random.randint(0, 0) for _ in range(3)]),
     height=256,width=256)
    n += 1

    
n = 0
for path_i in tqdm(mask_paths):
    dataset_nuclei_mask_train.add_image('source',
     n,path_i,bg_color = np.array([random.randint(0, 0) for _ in range(3)]),
     height=256,width=256)
    n += 1

mask_image = dataset_nuclei_mask_train.load_image(2)    
dataset_nuclei_train.load_mask(1, mask_image)

'''
image = dataset_nuclei_train.load_image(image_id)
mask, class_ids = dataset_nuclei_train.load_mask(image_id, mask_image)
'''

# Create model in training mode
model = modellib.MaskRCNN(mode="training", config=config,
                          model_dir=MODEL_DIR)

# Which weights to start with?
init_with = "coco"  # imagenet, coco, or last

if init_with == "imagenet":
    model.load_weights(model.get_imagenet_weights(), by_name=True)
elif init_with == "coco":
    # Load weights trained on MS COCO, but skip layers that
    # are different due to the different number of classes
    # See README for instructions to download the COCO weights
    model.load_weights(COCO_MODEL_PATH, by_name=True,
                       exclude=["mrcnn_class_logits", "mrcnn_bbox_fc", 
                                "mrcnn_bbox", "mrcnn_mask"])
elif init_with == "last":
    # Load the last model you trained and continue training
    model.load_weights(model.find_last()[1], by_name=True)
    
model.train(dataset_nuclei_train, dataset_nuclei_val, 
            learning_rate=config.LEARNING_RATE, 
            epochs=1, 
            layers='heads')

model.train(dataset_nuclei_train, dataset_nuclei_val, 
            learning_rate=config.LEARNING_RATE / 10,
            epochs=2, 
            layers="all")


model.keras_model.save_weights(model_path)
